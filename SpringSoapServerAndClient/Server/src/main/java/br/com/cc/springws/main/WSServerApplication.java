package br.com.cc.springws.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import br.com.cc.springws.config.WebServiceConfig;
import br.com.cc.springws.endpoint.CountryEndpoint;
import br.com.cc.springws.repository.CountryRepository;

@EnableAutoConfiguration
@ComponentScan(basePackageClasses={WebServiceConfig.class,CountryEndpoint.class,CountryRepository.class})
public class WSServerApplication  {
	
	public static void main(String[] args) {
		ApplicationContext applicationContext =  SpringApplication.run(WSServerApplication.class, args);
		
		
		for (String name : applicationContext.getBeanDefinitionNames()) {
			System.out.println("+ " + name);
		}
		
		
	}
}
