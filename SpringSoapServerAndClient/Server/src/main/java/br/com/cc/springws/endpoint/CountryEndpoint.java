package br.com.cc.springws.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import br.com.cc.springws.repository.CountryRepository;
import br.com.cc.springws.ws.types.GetCountryRequest;
import br.com.cc.springws.ws.types.GetCountryResponse;

@Endpoint
public class CountryEndpoint {

	public static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";
	
	//http://localhost:8080/ws/countries.wsdl
	
	@Autowired
	private CountryRepository countryRepository;

	
	
	
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCountryRequest")
	@ResponsePayload
	public GetCountryResponse getCountry(@RequestPayload GetCountryRequest request) {
		GetCountryResponse response = new GetCountryResponse();
		response.setCountry(countryRepository.findCountry(request.getName()));

		return response;
	}	
	
}
